package setting

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
)

type CsvTable struct {
	FileName string
	Records  []CsvRecord
}

type CsvRecord struct {
	Record map[string]string
}

func (c *CsvRecord) GetInt(field string) int {
	var r int
	var err error
	if r, err = strconv.Atoi(c.Record[field]); err != nil {
		panic(err)
	}
	return r
}

func (c *CsvRecord) GetString(field string) string {
	data, ok := c.Record[field]
	if ok {
		return data
	} else {
		Logger.Errorf("Get fileld failed! fileld:", field)
		return ""
	}
}

func (c *CsvRecord) GetBool(field string) bool {
	data, ok := c.Record[field]
	if ok {
		if data == "true" || data == "TRUE" {
			return true
		} else {
			return false
		}
	} else {
		Logger.Errorf("Get fileld failed! fileld:", field)
		return false
	}
}

func LoadCsvCfg(filename string, row int) *CsvTable {
	file, err := os.Open(filename)
	if err != nil {
		log.Println(err)
		return nil
	}
	defer file.Close()

	reader := csv.NewReader(file)
	if reader == nil {
		Logger.Errorf("NewReader return nil, file:", file)
		return nil
	}
	reader.FieldsPerRecord = -1
	records, err := reader.ReadAll()
	if err != nil {
		log.Printf("err,", err)
		return nil
	}
	if len(records) < row {
		log.Printf(filename, " is empty")
		return nil
	}
	//colNum := len(records[0])
	recordNum := len(records)
	var allRecords []CsvRecord
	for i := row; i < recordNum; i++ {
		record := &CsvRecord{make(map[string]string)}
		//按照行内容进行解析，因为单元格内容可能会空
		for k := 0; k < len(records[i]); k++ {
			record.Record[records[0][k]] = records[i][k]
		}
		allRecords = append(allRecords, *record)
	}
	var result = &CsvTable{
		filename,
		allRecords,
	}
	return result
}
