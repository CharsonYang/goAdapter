package httpServer

import (
	"goAdapter/httpServer/contorl"
	"goAdapter/httpServer/middleware"
	"goAdapter/setting"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

func RouterWeb(port string) {

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	//router := gin.New()

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	router.Static("/static", exeCurDir+"/webroot/static")
	router.Static("/plugin", exeCurDir+"/plugin/")
	router.Static("/layui", exeCurDir+"/webroot/layui")
	router.Static("/serialHelper", exeCurDir+"/webroot/serialHelper")

	router.StaticFile("/", exeCurDir+"/webroot/index.html")
	router.StaticFile("/favicon.ico", exeCurDir+"/webroot/favicon.ico")
	router.StaticFile("/serverConfig.json", exeCurDir+"/webroot/serverConfig.json")

	loginRouter := router.Group("/api/v1/system/")
	{
		loginRouter.POST("/login", contorl.ApiLogin)
	}

	router.Use(middleware.JWTAuth())
	{
		systemRouter := router.Group("/api/v1/system")
		{
			systemRouter.POST("/reboot", contorl.ApiSystemReboot)

			systemRouter.GET("/status", contorl.ApiGetSystemStatus)

			systemRouter.GET("/backup", contorl.ApiBackupFiles)

			systemRouter.POST("/recover", contorl.ApiRecoverFiles)

			systemRouter.POST("/update", contorl.ApiSystemUpdate)

			systemRouter.GET("/loginParam", contorl.ApiSystemLoginParam)

			systemRouter.GET("/MemUseList", contorl.ApiSystemMemoryUseList)

			systemRouter.GET("/DiskUseList", contorl.ApiSystemDiskUseList)

			systemRouter.GET("/DeviceOnlineList", contorl.ApiSystemDeviceOnlineList)

			systemRouter.GET("/DevicePacketLossList", contorl.ApiSystemDevicePacketLossList)

			systemRouter.POST("/systemRTC", contorl.ApiSystemSetSystemRTC)
		}

		logRouter := router.Group("/api/v1/log")
		{
			logRouter.GET("/param", contorl.ApiGetLogParam)

			logRouter.POST("/param", contorl.ApiSetLogParam)

			logRouter.GET("/filesInfo", contorl.ApiGetLogFilesInfo)

			logRouter.DELETE("/files", contorl.ApiDeleteLogFile)

			logRouter.GET("/file", contorl.ApiGetLogFile)
		}

		ntpRouter := router.Group("/api/v1/system/ntp")
		{
			ntpRouter.POST("/hostAddr", contorl.ApiSystemSetNTPHost)

			ntpRouter.GET("/hostAddr", contorl.ApiSystemGetNTPHost)
		}

		networkRouter := router.Group("/api/v1/network")
		{
			networkRouter.POST("/param", contorl.ApiAddNetwork)

			networkRouter.PUT("/param", contorl.ApiModifyNetwork)

			networkRouter.DELETE("/param", contorl.ApiDeleteNetwork)

			networkRouter.GET("/param", contorl.ApiGetNetwork)

			networkRouter.GET("/linkstate", contorl.ApiGetNetworkLinkState)

		}

		serialRouter := router.Group("/api/v1/serial")
		{

			serialRouter.GET("/param", contorl.ApiGetSerial)
		}

		deviceRouter := router.Group("/api/v1/device")
		{
			//增加采集接口
			deviceRouter.POST("/interface", contorl.ApiAddInterface)

			//修改采集接口
			deviceRouter.PUT("/interface", contorl.ApiModifyInterface)

			//删除采集接口
			deviceRouter.DELETE("/interface", contorl.ApiDeleteInterface)

			//获取接口信息
			deviceRouter.GET("/interface", contorl.ApiGetInterfaceInfo)

			//获取所有接口信息
			deviceRouter.GET("/allInterface", contorl.ApiGetAllInterfaceInfo)

			//向采集接口发送透传数据
			deviceRouter.POST("/interface/directData", contorl.ApiSendDirectDataToCollInterface)

			//增加节点
			deviceRouter.POST("/node", contorl.ApiAddNode)

			//修改单个节点
			deviceRouter.PUT("/node", contorl.ApiModifyNode)

			//修改多个节点
			deviceRouter.PUT("/nodes", contorl.ApiModifyNodes)

			//查看节点
			deviceRouter.GET("/node", contorl.ApiGetNode)

			//查看节点变量
			deviceRouter.GET("/nodeVariable", contorl.ApiGetNodeVariableFromCache)

			//查看节点历史变量
			deviceRouter.GET("/nodeHistoryVariable", contorl.ApiGetNodeHistoryVariableFromCache)

			//查看节点变量实时值
			deviceRouter.GET("/nodeRealVariable", contorl.ApiGetNodeReadVariable)

			//删除节点
			deviceRouter.DELETE("/node", contorl.ApiDeleteNode)

			//调用节点服务
			deviceRouter.POST("/service", contorl.ApiInvokeService)

			//增加设备模板
			//deviceRouter.POST("/template", ApiAddTemplate)

			//获取设备模板
			//deviceRouter.GET("/template", ApiGetTemplate)

			//增加设备物模型
			deviceRouter.POST("/tsl", contorl.ApiAddDeviceTSL)

			//删除设备物模型
			deviceRouter.DELETE("/tsl", contorl.ApiDeleteDeviceTSL)

			//修改设备物模型
			deviceRouter.PUT("/tsl", contorl.ApiModifyDeviceTSL)

			//查看设备物模型
			deviceRouter.GET("/tsl", contorl.ApiGetDeviceTSL)

			//查看设备物模型内容
			deviceRouter.GET("/tsl/contents", contorl.ApiGetDeviceTSLContents)

			//批量导入设备物模型内容
			deviceRouter.POST("/tsl/contents/csv", contorl.ApiImportDeviceTSLContents)

			//批量导出设备物模型内容
			deviceRouter.GET("/tsl/contents/csv", contorl.ApiExportDeviceTSLContents)

			//导出设备物模型内容模板
			deviceRouter.GET("/tsl/contents/template", contorl.ApiExportDeviceTSLContentsTemplate)

			//导入设备物模型插件
			deviceRouter.POST("/tsl/plugin", contorl.ApiImportDeviceTSLPlugin)

			//导出设备物模型插件
			deviceRouter.GET("/tsl/plugin", contorl.ApiExportDeviceTSLPlugin)

			//增加设备物模型属性
			deviceRouter.POST("/tsl/property", contorl.ApiAddDeviceTSLProperty)

			//修改设备物模型属性
			deviceRouter.PUT("/tsl/property", contorl.ApiModifyDeviceTSLProperty)

			//删除设备物模型属性
			deviceRouter.DELETE("/tsl/properties", contorl.ApiDeleteDeviceTSLProperties)

			//查看设备物模型属性
			deviceRouter.GET("/tsl/properties", contorl.ApiGetDeviceTSLProperties)

			//增加设备物模型服务
			deviceRouter.POST("/tsl/service", contorl.ApiAddDeviceTSLService)

			//修改设备物模型服务
			deviceRouter.PUT("/tsl/service", contorl.ApiModifyDeviceTSLService)

			//删除设备物模型服务
			deviceRouter.DELETE("/tsl/services", contorl.ApiDeleteDeviceTSLServices)

			//获取通信接口
			deviceRouter.GET("/commInterface", contorl.ApiGetCommInterface)

			//增加通信接口
			deviceRouter.POST("/commInterface", contorl.ApiAddCommInterface)

			//修改通信接口
			deviceRouter.PUT("/commInterface", contorl.ApiModifyCommInterface)

			//删除通信接口
			deviceRouter.DELETE("/commInterface", contorl.ApiDeleteCommInterface)

			//增加串口通信接口
			deviceRouter.POST("/commSerialInterface", contorl.ApiAddCommSerialInterface)

			//修改串口通信接口
			deviceRouter.PUT("/commSerialInterface", contorl.ApiModifyCommSerialInterface)

			//删除串口通信接口
			deviceRouter.DELETE("/commSerialInterface", contorl.ApiDeleteCommSerialInterface)
		}

		toolRouter := router.Group("/api/v1/tool")
		{
			//获取通信报文
			toolRouter.POST("/commMessage", contorl.ApiGetCommMessage)
		}

		pluginRouter := router.Group("/api/v1/update")
		{
			pluginRouter.POST("/plugin", contorl.ApiUpdatePlugin)
		}

		reportRouter := router.Group("/api/v1/report")
		{
			reportRouter.POST("/param", contorl.ApiSetReportGWParam)

			reportRouter.GET("/param", contorl.ApiGetReportGWParam)

			reportRouter.DELETE("/param", contorl.ApiDeleteReportGWParam)

			reportRouter.POST("/node/param", contorl.ApiSetReportNodeWParam)

			reportRouter.POST("/nodes/param", contorl.ApiBatchAddReportNodeParam)

			reportRouter.GET("/node/param", contorl.ApiGetReportNodeWParam)

			reportRouter.DELETE("/node/param", contorl.ApiDeleteReportNodeWParam)
		}
	}

	if err := router.Run(port); err != nil {
		setting.Logger.Errorf("gin run err,%v", err)
		os.Exit(0)
	}
}
