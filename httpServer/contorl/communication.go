package contorl

import (
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/device/commInterface"
	"goAdapter/setting"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ApiAddCommInterface(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	// fmt.Println(string(bodyBuf[:n]))

	var Param json.RawMessage
	interfaceInfo := struct {
		Name  string           `json:"Name"` // 接口名称
		Type  string           `json:"Type"` // 接口类型,比如serial,TcpClient,udp,http
		Param *json.RawMessage `json:"Param"`
	}{
		Param: &Param,
	}

	err := json.Unmarshal(bodyBuf[:n], &interfaceInfo)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	switch interfaceInfo.Type {
	case "LocalSerial":
		serial := commInterface.SerialInterfaceParam{}
		err = json.Unmarshal(Param, &serial)
		if err != nil {
			setting.Logger.Errorf("CommunicationSerialInterface json unMarshall err ", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", serial)
		SerialInterface := &commInterface.CommunicationSerialTemplate{
			Param: serial,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		for _, v := range commInterface.CommunicationSerialMap {
			if v.Param.Name == SerialInterface.Param.Name {
				aParam.Code = "1"
				aParam.Message = "serialName is exist"
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
		SerialInterface.Open()
		commInterface.CommunicationSerialMap = append(commInterface.CommunicationSerialMap, SerialInterface)
		commInterface.WriteCommSerialInterfaceListToJson()
		commInterface.CommunicationInterfaceMap = append(commInterface.CommunicationInterfaceMap, SerialInterface)
	case "TcpClient":
		TcpClient := commInterface.TcpClientInterfaceParam{}
		err = json.Unmarshal(Param, &TcpClient)
		if err != nil {
			setting.Logger.Errorf("CommunicationTcpClientInterface json unMarshall err,%v", err)
			break
		}
		setting.Logger.Debugf("type %+v", TcpClient)
		TcpClientInterface := &commInterface.CommunicationTcpClientTemplate{
			Param: TcpClient,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		for _, v := range commInterface.CommunicationTcpClientMap {
			if (v.Param.Port == TcpClientInterface.Param.Port) && (v.Param.IP == TcpClientInterface.Param.IP) {
				aParam.Code = "1"
				aParam.Message = "地址和端口已经存在"
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
		//增加通信接口立马生效
		TcpClientInterface.Open()
		commInterface.CommunicationTcpClientMap = append(commInterface.CommunicationTcpClientMap, TcpClientInterface)
		commInterface.WriteCommTcpClientInterfaceListToJson()
		commInterface.CommunicationInterfaceMap = append(commInterface.CommunicationInterfaceMap, TcpClientInterface)
	case "TcpServer":
		TcpServer := commInterface.TcpServerInterfaceParam{}
		err = json.Unmarshal(Param, &TcpServer)
		if err != nil {
			setting.Logger.Errorf("CommunicationTcpServerInterface json unMarshall err,%v", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", TcpServer)
		TcpServerInterface := &commInterface.CommunicationTcpServerTemplate{
			Param: TcpServer,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		for _, v := range commInterface.CommunicationTcpServerMap {
			if v.Param.Port == TcpServerInterface.Param.Port {
				aParam.Code = "1"
				aParam.Message = "TcpServer Port is exist"
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
		TcpServerInterface.Open()
		commInterface.CommunicationTcpServerMap = append(commInterface.CommunicationTcpServerMap, TcpServerInterface)
		commInterface.WriteCommTcpServerInterfaceListToJson()
		commInterface.CommunicationInterfaceMap = append(commInterface.CommunicationInterfaceMap, TcpServerInterface)
	case "IoOut":
		IoOut := commInterface.IoOutInterfaceParam{}
		err = json.Unmarshal(Param, &IoOut)
		if err != nil {
			setting.Logger.Errorf("CommunicationIoOutInterface json unMarshall err,", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", IoOut)
		IoOutInterface := &commInterface.CommunicationIoOutTemplate{
			Param: IoOut,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		for _, v := range commInterface.CommunicationIoOutMap {
			if v.Param.Name == IoOutInterface.Param.Name {
				aParam.Code = "1"
				aParam.Message = "IoOut Name is exist"
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
		IoOutInterface.Open()
		commInterface.CommunicationIoOutMap = append(commInterface.CommunicationIoOutMap, IoOutInterface)
		commInterface.WriteCommIoOutInterfaceListToJson()
		commInterface.CommunicationInterfaceMap = append(commInterface.CommunicationInterfaceMap, IoOutInterface)
	case "IoIn":
		IoIn := commInterface.IoInInterfaceParam{}
		err = json.Unmarshal(Param, &IoIn)
		if err != nil {
			setting.Logger.Errorf("CommunicationIoInInterface json unMarshall err,", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", IoIn)
		IoInInterface := &commInterface.CommunicationIoInTemplate{
			Param: IoIn,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		for _, v := range commInterface.CommunicationIoInMap {
			if v.Param.Name == IoInInterface.Param.Name {
				aParam.Code = "1"
				aParam.Message = "IoIn Name is exist"
				aParam.Data = ""
				sJson, _ := json.Marshal(aParam)
				context.String(http.StatusOK, string(sJson))
				return
			}
		}
		IoInInterface.Open()
		commInterface.CommunicationIoInMap = append(commInterface.CommunicationIoInMap, IoInInterface)
		commInterface.WriteCommIoInInterfaceListToJson()
		commInterface.CommunicationInterfaceMap = append(commInterface.CommunicationInterfaceMap, IoInInterface)
	}

	aParam.Code = "0"
	aParam.Message = ""
	aParam.Data = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiModifyCommInterface(context *gin.Context) {
	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)
	// fmt.Println(string(bodyBuf[:n]))

	var Param json.RawMessage
	interfaceInfo := struct {
		Name  string           `json:"Name"` // 接口名称
		Type  string           `json:"Type"` // 接口类型,比如serial,TcpClient,udp,http
		Param *json.RawMessage `json:"Param"`
	}{
		Param: &Param,
	}

	err := json.Unmarshal(bodyBuf[:n], &interfaceInfo)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	switch interfaceInfo.Type {
	case "LocalSerial":
		serial := commInterface.SerialInterfaceParam{}
		err = json.Unmarshal(Param, &serial)
		if err != nil {
			setting.Logger.Errorf("CommunicationSerialInterface json unMarshall err,", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", serial)
		SerialInterface := &commInterface.CommunicationSerialTemplate{
			Param: serial,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}
		index := -1
		for k, v := range commInterface.CommunicationSerialMap {
			if v.Name == SerialInterface.Name {
				index = k
				v.Close()
			}
		}
		if index != -1 {
			commInterface.CommunicationSerialMap[index].Close()
			for _, v := range device.CollectInterfaceMap.Coll {
				if v.CommInterfaceName == SerialInterface.Name {
					v.CommInterface = SerialInterface
					v.CommInterfaceUpdate <- true
				}
			}
			commInterface.CommunicationSerialMap[index] = SerialInterface
			commInterface.WriteCommSerialInterfaceListToJson()
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	case "TcpClient":
		TcpClient := commInterface.TcpClientInterfaceParam{}
		err = json.Unmarshal(Param, &TcpClient)
		if err != nil {
			setting.Logger.Errorf("CommunicationTcpClientInterface json unMarshall err,%v", err)
			break
		}
		setting.Logger.Debugf("type %+v", TcpClient)
		TcpClientInterface := &commInterface.CommunicationTcpClientTemplate{
			Param: TcpClient,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		index := -1
		for k, v := range commInterface.CommunicationTcpClientMap {
			if v.Name == TcpClientInterface.Name {
				index = k
				v.Close()
				break
			}
		}
		if index != -1 {
			for _, v := range device.CollectInterfaceMap.Coll {
				if v.CommInterfaceName == TcpClientInterface.Name {
					v.CommInterface = TcpClientInterface
					v.CommInterfaceUpdate <- true
				}
			}
			commInterface.CommunicationTcpClientMap[index] = TcpClientInterface
			commInterface.WriteCommTcpClientInterfaceListToJson()
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	case "TcpServer":
		TcpServer := commInterface.TcpServerInterfaceParam{}
		err = json.Unmarshal(Param, &TcpServer)
		if err != nil {
			setting.Logger.Errorf("CommunicationTcpServerInterface json unMarshall err,%v", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", TcpServer)
		TcpServerInterface := &commInterface.CommunicationTcpServerTemplate{
			Param: TcpServer,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		index := -1
		for k, v := range commInterface.CommunicationTcpServerMap {
			if v.Name == TcpServerInterface.Name {
				index = k
				v.Close()
			}
		}
		if index != -1 {
			commInterface.CommunicationTcpServerMap[index] = TcpServerInterface
			for _, v := range device.CollectInterfaceMap.Coll {
				if v.CommInterfaceName == TcpServerInterface.Name {
					v.CommInterface = TcpServerInterface
					v.CommInterfaceUpdate <- true
				}
			}
			commInterface.WriteCommTcpServerInterfaceListToJson()
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	case "IoOut":
		IoOut := commInterface.IoOutInterfaceParam{}
		err = json.Unmarshal(Param, &IoOut)
		if err != nil {
			setting.Logger.Errorf("CommunicationIoOutInterface json unMarshall err,%v", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", IoOut)
		IoOutInterface := &commInterface.CommunicationIoOutTemplate{
			Param: IoOut,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}

		index := -1
		for k, v := range commInterface.CommunicationIoOutMap {
			if v.Name == IoOutInterface.Name {
				index = k
				v.Close()
			}
		}
		if index != -1 {
			commInterface.CommunicationIoOutMap[index] = IoOutInterface
			for _, v := range device.CollectInterfaceMap.Coll {
				if v.CommInterfaceName == IoOutInterface.Name {
					v.CommInterface = IoOutInterface
					v.CommInterfaceUpdate <- true
				}
			}
			commInterface.WriteCommIoOutInterfaceListToJson()
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	case "IoIn":
		IoIn := commInterface.IoInInterfaceParam{}
		err = json.Unmarshal(Param, &IoIn)
		if err != nil {
			setting.Logger.Errorf("CommunicationIoInInterface json unMarshall err,%v", err)
			break
		}
		setting.Logger.Debugf("type %+v\n", IoIn)
		IoInInterface := &commInterface.CommunicationIoInTemplate{
			Param: IoIn,
			Name:  interfaceInfo.Name,
			Type:  interfaceInfo.Type,
		}
		index := -1
		for k, v := range commInterface.CommunicationIoInMap {
			if v.Name == IoInInterface.Name {
				index = k
				v.Close()
			}
		}
		if index != -1 {
			commInterface.CommunicationIoInMap[index] = IoInInterface
			for _, v := range device.CollectInterfaceMap.Coll {
				if v.CommInterfaceName == IoInInterface.Name {
					v.CommInterface = IoInInterface
					v.CommInterfaceUpdate <- true
				}
			}
			commInterface.WriteCommIoInInterfaceListToJson()
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "commInterface is not exist"
	aParam.Data = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteCommInterface(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	cName := context.Query("commInterface")

	for _, v := range device.CollectInterfaceMap.Coll {
		if v.CommInterfaceName == cName {
			aParam.Code = "1"
			aParam.Message = "通信接口被采集接口使用，不可以删除"
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for k, v := range commInterface.CommunicationSerialMap {
		if v.Name == cName {
			v.Close()
			commInterface.CommunicationSerialMap = append(commInterface.CommunicationSerialMap[:k], commInterface.CommunicationSerialMap[k+1:]...)
			commInterface.WriteCommSerialInterfaceListToJson()
			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for k, v := range commInterface.CommunicationTcpClientMap {
		if v.Name == cName {
			v.Close()
			commInterface.CommunicationTcpClientMap = append(commInterface.CommunicationTcpClientMap[:k], commInterface.CommunicationTcpClientMap[k+1:]...)
			commInterface.WriteCommTcpClientInterfaceListToJson()

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for k, v := range commInterface.CommunicationTcpServerMap {
		if v.Name == cName {
			v.Close()
			commInterface.CommunicationTcpServerMap = append(commInterface.CommunicationTcpServerMap[:k], commInterface.CommunicationTcpServerMap[k+1:]...)
			commInterface.WriteCommTcpServerInterfaceListToJson()

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for k, v := range commInterface.CommunicationIoOutMap {
		if v.Name == cName {
			v.Close()
			commInterface.CommunicationIoOutMap = append(commInterface.CommunicationIoOutMap[:k], commInterface.CommunicationIoOutMap[k+1:]...)
			commInterface.WriteCommIoOutInterfaceListToJson()

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	for k, v := range commInterface.CommunicationIoInMap {
		if v.Name == cName {
			v.Close()
			commInterface.CommunicationIoInMap = append(commInterface.CommunicationIoInMap[:k], commInterface.CommunicationIoInMap[k+1:]...)
			commInterface.WriteCommIoInInterfaceListToJson()

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "commInterface is not exist"
	aParam.Data = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiGetCommInterface(context *gin.Context) {

	type CommunicationInterfaceTemplate struct {
		Name  string      `json:"Name"`  // 接口名称
		Type  string      `json:"Type"`  // 接口类型,比如serial,TcpClient,udp,http
		Param interface{} `json:"Param"` // 接口参数
	}

	type CommunicationInterfaceManageTemplate struct {
		InterfaceCnt int
		InterfaceMap []CommunicationInterfaceTemplate
	}

	aParam := &struct {
		Code    string
		Message string
		Data    CommunicationInterfaceManageTemplate
	}{}

	CommunicationInterfaceManage := CommunicationInterfaceManageTemplate{
		InterfaceCnt: 0,
		InterfaceMap: make([]CommunicationInterfaceTemplate, 0),
	}

	aParam.Code = "0"
	aParam.Message = ""
	for _, v := range commInterface.CommunicationSerialMap {
		CommunicationInterface := CommunicationInterfaceTemplate{
			Name:  v.Name,
			Type:  v.Type,
			Param: v.Param,
		}
		CommunicationInterfaceManage.InterfaceCnt++
		CommunicationInterfaceManage.InterfaceMap = append(CommunicationInterfaceManage.InterfaceMap,
			CommunicationInterface)
	}

	for _, v := range commInterface.CommunicationTcpClientMap {
		CommunicationInterface := CommunicationInterfaceTemplate{
			Name:  v.Name,
			Type:  v.Type,
			Param: v.Param,
		}
		CommunicationInterfaceManage.InterfaceCnt++
		CommunicationInterfaceManage.InterfaceMap = append(CommunicationInterfaceManage.InterfaceMap,
			CommunicationInterface)
	}

	for _, v := range commInterface.CommunicationTcpServerMap {
		CommunicationInterface := CommunicationInterfaceTemplate{
			Name:  v.Name,
			Type:  v.Type,
			Param: v.Param,
		}
		CommunicationInterfaceManage.InterfaceCnt++
		CommunicationInterfaceManage.InterfaceMap = append(CommunicationInterfaceManage.InterfaceMap,
			CommunicationInterface)
	}

	for _, v := range commInterface.CommunicationIoOutMap {
		CommunicationInterface := CommunicationInterfaceTemplate{
			Name:  v.Name,
			Type:  v.Type,
			Param: v.Param,
		}
		CommunicationInterfaceManage.InterfaceCnt++
		CommunicationInterfaceManage.InterfaceMap = append(CommunicationInterfaceManage.InterfaceMap,
			CommunicationInterface)
	}

	for _, v := range commInterface.CommunicationIoInMap {
		CommunicationInterface := CommunicationInterfaceTemplate{
			Name:  v.Name,
			Type:  v.Type,
			Param: v.Param,
		}
		CommunicationInterfaceManage.InterfaceCnt++
		CommunicationInterfaceManage.InterfaceMap = append(CommunicationInterfaceManage.InterfaceMap,
			CommunicationInterface)
	}

	aParam.Data = CommunicationInterfaceManage

	sJson, _ := json.Marshal(aParam)

	context.String(http.StatusOK, string(sJson))
}

func ApiAddCommSerialInterface(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	interfaceInfo := struct {
		Name  string                             `json:"Name"` // 接口名称
		Type  string                             `json:"Type"` // 接口类型,比如serial,TcpClient,udp,http
		Param commInterface.SerialInterfaceParam `json:"Param"`
	}{}

	err := json.Unmarshal(bodyBuf[:n], &interfaceInfo)
	if err != nil {
		fmt.Println("interfaceInfo json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for _, v := range commInterface.CommunicationSerialMap {
		// 判断通信接口名称是否一致
		if (v.Name == interfaceInfo.Name) || (v.Param.Name == interfaceInfo.Param.Name) {
			aParam.Code = "1"
			aParam.Message = "name is exist"
			aParam.Data = ""

			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	SerialInterface := &commInterface.CommunicationSerialTemplate{
		Param: interfaceInfo.Param,
		Name:  interfaceInfo.Name,
		Type:  interfaceInfo.Type,
	}

	commInterface.CommunicationSerialMap = append(commInterface.CommunicationSerialMap, SerialInterface)
	commInterface.WriteCommSerialInterfaceListToJson()

	aParam.Code = "0"
	aParam.Message = ""
	aParam.Data = ""
	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiModifyCommSerialInterface(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	interfaceInfo := struct {
		Name  string                             `json:"Name"` // 接口名称
		Type  string                             `json:"Type"` // 接口类型,比如serial,TcpClient,udp,http
		Param commInterface.SerialInterfaceParam `json:"Param"`
	}{}

	err := json.Unmarshal(bodyBuf[:n], &interfaceInfo)
	if err != nil {
		fmt.Println("CommSerialInterface json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for k, v := range commInterface.CommunicationSerialMap {
		// 判断名称是否一致
		if v.Name == interfaceInfo.Name {
			commInterface.CommunicationSerialMap[k].Type = interfaceInfo.Type
			commInterface.CommunicationSerialMap[k].Param = interfaceInfo.Param
			commInterface.WriteCommSerialInterfaceListToJson()

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "addr is not exist"
	aParam.Data = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}

func ApiDeleteCommSerialInterface(context *gin.Context) {

	aParam := struct {
		Code    string `json:"Code"`
		Message string `json:"Message"`
		Data    string `json:"Data"`
	}{
		Code:    "1",
		Message: "",
		Data:    "",
	}

	bodyBuf := make([]byte, 1024)
	n, _ := context.Request.Body.Read(bodyBuf)

	interfaceInfo := struct {
		Name string `json:"Name"` // 接口名称
		Type string `json:"Type"` // 接口类型,比如serial,TcpClient,udp,http
	}{}

	err := json.Unmarshal(bodyBuf[:n], &interfaceInfo)
	if err != nil {
		fmt.Println("CommSerialInterface json unMarshall err,", err)

		aParam.Code = "1"
		aParam.Message = "json unMarshall err"
		sJson, _ := json.Marshal(aParam)
		context.String(http.StatusOK, string(sJson))
		return
	}

	for k, v := range commInterface.CommunicationSerialMap {
		// 判断名称是否一致
		if v.Name == interfaceInfo.Name {
			commInterface.CommunicationSerialMap = append(commInterface.CommunicationSerialMap[:k], commInterface.CommunicationSerialMap[k+1:]...)
			commInterface.WriteCommSerialInterfaceListToJson()

			aParam.Code = "0"
			aParam.Message = ""
			aParam.Data = ""
			sJson, _ := json.Marshal(aParam)
			context.String(http.StatusOK, string(sJson))
			return
		}
	}

	aParam.Code = "1"
	aParam.Message = "addr is not exist"
	aParam.Data = ""

	sJson, _ := json.Marshal(aParam)
	context.String(http.StatusOK, string(sJson))
}
