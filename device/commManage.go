package device

import (
	"context"
	"fmt"
	"goAdapter/device/commInterface"
	"goAdapter/device/eventBus"
	"goAdapter/setting"
	"strconv"
	"time"

	lua "github.com/yuin/gopher-lua"
)

type CommunicationCmdTemplate struct {
	CollInterfaceName string //采集接口名称
	DeviceName        string //采集接口下设备名称
	FunName           string
	FunPara           string
}

type CommunicationDirectDataReqTemplate struct {
	CollInterfaceName string //采集接口名称
	Data              []byte
}

type CommunicationDirectDataAckTemplate struct {
	CollInterfaceName string //采集接口名称
	Status            bool
	Data              []byte
}

type CommunicationMessageTemplate struct {
	CollName  string `json:"CollInterfaceName"` //接口名称
	TimeStamp string `json:"TimeStamp"`         //时间戳
	Direction string `json:"DataDirection"`     //数据方向
	Content   string `json:"DataContent"`       //数据内容
}

type CommunicationRxTemplate struct {
	Status bool
	RxBuf  []byte
}

type CommunicationManageTemplate struct {
	EmergencyRequestChan  chan CommunicationCmdTemplate
	EmergencyAckChan      chan CommunicationRxTemplate
	CommonRequestChan     chan CommunicationCmdTemplate
	DirectDataRequestChan chan CommunicationDirectDataReqTemplate
	DirectDataAckChan     chan CommunicationDirectDataAckTemplate
	PacketChan            chan []byte
	CommMessage           []CommunicationMessageTemplate
	QuitChan              chan bool
}

const (
	CommunicationManageMessageMaxCnt = 100
)

const (
	CommunicationState_Start int = iota
	CommunicationState_Generate
	CommunicationState_Send       //命令发送
	CommunicationState_Wait       //命令等待接收
	CommunicationState_WaitSucess //命令接收成功
	CommunicationState_WaitFail   //命令接收失败
	CommunicationState_Stop
	CommunicationState_DirectDataSend //透传数据发送
	CommunicationState_DirectDataWait //透传数据等待接收
	CommunicationState_DirectDataStop //透传数据任务停止
)

func NewCommunicationManageTemplate() *CommunicationManageTemplate {

	template := &CommunicationManageTemplate{
		EmergencyRequestChan:  make(chan CommunicationCmdTemplate, 1),
		CommonRequestChan:     make(chan CommunicationCmdTemplate, 100),
		EmergencyAckChan:      make(chan CommunicationRxTemplate, 1),
		DirectDataRequestChan: make(chan CommunicationDirectDataReqTemplate, 1),
		DirectDataAckChan:     make(chan CommunicationDirectDataAckTemplate, 1),
		PacketChan:            make(chan []byte, 100), //最多连续接收100帧数据
		CommMessage:           make([]CommunicationMessageTemplate, 0),
		QuitChan:              make(chan bool),
	}

	return template
}

func (c *CommunicationManageTemplate) CommunicationManageMessageAdd(coll string, dir string, buf []byte) {
	CommunicationMessage := CommunicationMessageTemplate{
		CollName:  coll,
		TimeStamp: time.Now().Format("2006-01-02 15:04:05"),
		Direction: dir,
		Content:   fmt.Sprintf("%X", buf),
	}
	if len(c.CommMessage) < CommunicationManageMessageMaxCnt {
		c.CommMessage = append(c.CommMessage, CommunicationMessage)
	} else {
		c.CommMessage = c.CommMessage[1:]
		c.CommMessage = append(c.CommMessage, CommunicationMessage)
	}
}

func (c *CommunicationManageTemplate) CommunicationManageAddCommon(cmd CommunicationCmdTemplate) {

	c.CommonRequestChan <- cmd
}

func (c *CommunicationManageTemplate) CommunicationManageAddEmergency(cmd CommunicationCmdTemplate) CommunicationRxTemplate {

	c.EmergencyRequestChan <- cmd

	return <-c.EmergencyAckChan
}

func (c *CommunicationManageTemplate) CommunicationManageAddDirectData(req CommunicationDirectDataReqTemplate) CommunicationDirectDataAckTemplate {

	c.DirectDataRequestChan <- req

	return <-c.DirectDataAckChan
}

func (c *CommunicationManageTemplate) CommunicationManageProcessReceiveData(ctx context.Context, comm commInterface.CommunicationInterface) {

	//阻塞读
	rxBuf := make([]byte, 1024)
	rxBufCnt := 0

	for {
		select {
		case <-ctx.Done():
			setting.Logger.Debugf("%s exit", comm.GetName())
			return
		case <-c.QuitChan:
			setting.Logger.Debugf("%s exit", comm.GetName())
			return
		default:
			//阻塞读
			rxBufCnt = comm.ReadData(rxBuf)
			if rxBufCnt > 0 {
				//setting.Logger.Debugf("%s:curRxBufCnt %v", collName, rxBufCnt)
				//setting.Logger.Debugf("%s:CurRxBuf %X", collName, rxBuf[:rxBufCnt])

				//追加接收的数据到接收缓冲区
				//rxTotalBuf = append(rxTotalBuf, rxBuf[:rxBufCnt]...)
				c.PacketChan <- rxBuf[:rxBufCnt]
				//log.Printf("chanLen %v\n", len(c.PacketChan))

				//清除本次接收数据
				rxBufCnt = 0
			}
			time.Sleep(100 * time.Millisecond)
		}
	}
}

func (c *CommunicationManageTemplate) CommunicationStateMachine(cmd CommunicationCmdTemplate,
	collName string,
	commInterface commInterface.CommunicationInterface,
	node *DeviceNodeTemplate,
	eventBus *eventBus.Bus,
	lStateMap map[string]*lua.LState,
	offLineCnt int) CommunicationRxTemplate {
	rxResult := CommunicationRxTemplate{
		Status: false,
	}

	commState := CommunicationState_Start

	commStep := 0
	txBuf := make([]byte, 0)
	continues := false
	startT := time.Now() //计算当前时间
	func() {
		for {
			switch commState {
			case CommunicationState_Start:
				{
					commState = CommunicationState_Generate
				}
			case CommunicationState_Generate:
				{
					//--------------组包---------------------------
					result := false
					if cmd.FunName == "GetDeviceRealVariables" {
						txBuf, result, continues = node.GenerateGetRealVariables(lStateMap[node.Type], node.Addr, commStep)
						if result == false {
							setting.Logger.Errorf("%v:GetRealVariables fail", collName)
							commState = CommunicationState_Stop
						} else {
							commState = CommunicationState_Send
							commStep++
						}
					} else {
						txBuf, result, continues = node.DeviceCustomCmd(lStateMap[node.Type], node.Addr,
							cmd.FunName,
							cmd.FunPara,
							commStep)
						if result == false {
							setting.Logger.Errorf("%v:DeviceCustomCmd fail", collName)
							commState = CommunicationState_Stop
						} else {
							commState = CommunicationState_Send
							commStep++
						}
					}
				}
			case CommunicationState_Send:
				{
					//---------------发送-------------------------
					commInterface.WriteData(txBuf)
					node.CommTotalCnt++
					setting.Logger.Debugf("%v:txbuf %X", collName, txBuf)
					c.CommunicationManageMessageAdd(collName, "send", txBuf)
					commState = CommunicationState_Wait
				}
			case CommunicationState_Wait:
				{
					//阻塞读
					rxBuf := make([]byte, 256)
					rxTotalBuf := make([]byte, 0)
					rxBufCnt := 0
					rxTotalBufCnt := 0
					var timeout int
					timeout, _ = strconv.Atoi(commInterface.GetTimeOut())
					timerOut := time.NewTimer(time.Duration(timeout) * time.Millisecond)
					func() {
						for {
							select {
							//继续接收数据
							case rxBuf = <-c.PacketChan:
								{
									rxBufCnt = len(rxBuf)
									if rxBufCnt > 0 {
										rxTotalBufCnt += rxBufCnt
										//追加接收的数据到接收缓冲区
										rxTotalBuf = append(rxTotalBuf, rxBuf[:rxBufCnt]...)
										//清除本次接收数据
										rxBufCnt = 0
										rxBuf = rxBuf[0:0]
									}
								}
							//是否接收超时
							case <-timerOut.C:
								{
									timerOut.Stop()
									c.CommunicationManageMessageAdd(collName, "receive", rxTotalBuf)
									setting.Logger.Debugf("%v:rx timeout rxbuf:%X", collName, rxTotalBuf)

									node.CurCommFailCnt++
									if node.CurCommFailCnt >= offLineCnt {
										node.CurCommFailCnt = 0
										//设备从上线变成离线
										if node.CommStatus == "onLine" {
											content := CollectInterfaceEventTemplate{
												Topic:    "offLine",
												CollName: collName,
												NodeName: node.Name,
												Content:  node.Name,
											}
											err := eventBus.Publish("offLine", content)
											if err != nil {
												setting.Logger.Debugf("%v:publish offLine", collName)
											}
										}
										node.CommStatus = "offLine"
									}
									rxTotalBufCnt = 0
									rxTotalBuf = rxTotalBuf[0:0]

									commState = CommunicationState_WaitFail
									return
								}
							//是否正确收到数据包
							case rxStatus := <-node.AnalysisRx(lStateMap[node.Type], node.Addr,
								node.Properties, rxTotalBuf, rxTotalBufCnt):
								{
									timerOut.Stop()
									setting.Logger.Debugf("%v:rx ok rxbuf %X", collName, rxTotalBuf)

									rxResult.Status = rxStatus
									rxResult.RxBuf = rxTotalBuf

									c.CommunicationManageMessageAdd(collName, "receive", rxTotalBuf)

									//设备从离线变成上线
									if node.CommStatus == "offLine" {
										content := CollectInterfaceEventTemplate{
											Topic:    "onLine",
											CollName: collName,
											NodeName: node.Name,
											Content:  node.Name,
										}
										err := eventBus.Publish("onLine", content)
										if err != nil {
											setting.Logger.Debugf("%v: publish onLine", collName)
										}
									}

									if continues == false {
										content := CollectInterfaceEventTemplate{
											Topic:    "update",
											CollName: collName,
											NodeName: node.Name,
											Content:  node.Name,
										}
										err := eventBus.Publish("update", content)
										if err != nil {
											setting.Logger.Debugf("%v: publish update", collName)
										}
									}

									node.CommSuccessCnt++
									node.CurCommFailCnt = 0
									node.CommStatus = "onLine"
									node.LastCommRTC = time.Now().Format("2006-01-02 15:04:05")

									rxTotalBufCnt = 0
									rxTotalBuf = rxTotalBuf[0:0]
									commState = CommunicationState_WaitSucess
									return
								}
							}
						}
					}()
				}
			case CommunicationState_WaitSucess:
				{
					//通信帧延时
					var interval int
					interval, _ = strconv.Atoi(commInterface.GetInterval())
					time.Sleep(time.Duration(interval) * time.Millisecond)
					commState = CommunicationState_Stop
				}
			case CommunicationState_WaitFail:
				{
					commState = CommunicationState_Stop
				}
			case CommunicationState_Stop:
				{
					tc := time.Since(startT) //计算耗时
					setting.Logger.Debugf("%v time cost = %v", collName, tc)
					if continues == true {
						commState = CommunicationState_Start
					} else {
						return
					}
				}
			}
		}
	}()

	return rxResult
}

func (c *CommunicationManageTemplate) CommunicationDirectDataStateMachine(req CommunicationDirectDataReqTemplate, commInterface commInterface.CommunicationInterface) CommunicationDirectDataAckTemplate {

	ack := CommunicationDirectDataAckTemplate{
		Status: false,
		Data:   make([]byte, 0),
	}

	commState := CommunicationState_DirectDataSend

	func() {
		for {
			switch commState {
			case CommunicationState_DirectDataSend:
				{
					//---------------发送-------------------------
					commInterface.WriteData(req.Data)
					commState = CommunicationState_DirectDataWait
				}
			case CommunicationState_DirectDataWait:
				{
					//阻塞读
					rxBuf := make([]byte, 256)
					rxTotalBuf := make([]byte, 0)
					rxBufCnt := 0
					rxTotalBufCnt := 0
					var timeout int
					timeout, _ = strconv.Atoi(commInterface.GetTimeOut())
					timerOut := time.NewTimer(time.Duration(timeout) * time.Millisecond)
					func() {
						for {
							select {
							//继续接收数据
							case rxBuf = <-c.PacketChan:
								{
									rxBufCnt = len(rxBuf)
									if rxBufCnt > 0 {
										rxTotalBufCnt += rxBufCnt
										//追加接收的数据到接收缓冲区
										rxTotalBuf = append(rxTotalBuf, rxBuf[:rxBufCnt]...)
										//清除本次接收数据
										rxBufCnt = 0
										rxBuf = rxBuf[0:0]
									}
								}
							//是否接收超时
							case <-timerOut.C:
								{
									timerOut.Stop()
									ack.Data = append(ack.Data, rxTotalBuf[:rxTotalBufCnt]...)
									if len(ack.Data) == 0 {
										ack.Status = false
									} else {
										ack.Status = true
									}
									rxTotalBufCnt = 0
									rxTotalBuf = rxTotalBuf[0:0]
									commState = CommunicationState_DirectDataStop
									return
								}
							}
						}
					}()
				}
			case CommunicationState_DirectDataStop:
				{
					return
				}
			}
		}
	}()

	return ack
}
