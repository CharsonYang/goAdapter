package commInterface

import (
	"encoding/json"
	"goAdapter/setting"
	"log"
	"net"
	"os"
	"path/filepath"
)

type TcpServerInterfaceParam struct {
	IP       string `json:"IP"`
	Port     string `json:"Port"`
	Timeout  string `json:"Timeout"`  //通信超时
	Interval string `json:"Interval"` //通信间隔
}

type CommunicationTcpServerTemplate struct {
	Name  string                  `json:"Name"`  //接口名称
	Type  string                  `json:"Type"`  //接口类型,比如serial,TcpClient,TcpServer,udp,http
	Param TcpServerInterfaceParam `json:"Param"` //接口参数
	Conn  net.Conn                `json:"-"`     //通信句柄
}

var CommunicationTcpServerMap = make([]*CommunicationTcpServerTemplate, 0)

func (c *CommunicationTcpServerTemplate) Open() bool {
	listener, err := net.Listen("tcp", c.Param.IP+":"+c.Param.Port)
	if err != nil {
		setting.Logger.Errorf("%s TcpServer open err %v", c.Name, err)
		return false
	} else {
		setting.Logger.Debugf("%s TcpServer open ok", c.Name)
	}

	go c.Accept(listener)

	return true
}

func (c *CommunicationTcpServerTemplate) Accept(listener net.Listener) bool {

	for {
		conn, err := listener.Accept()
		if err != nil {
			setting.Logger.Errorf("%s TcpServer Accept err %v", c.Name, err)
			continue
		}
		c.Close()
		c.Conn = conn
	}
}

func (c *CommunicationTcpServerTemplate) Close() bool {
	if c.Conn != nil {
		err := c.Conn.Close()
		if err != nil {
			setting.Logger.Errorf("%s TcpServer Close err %v", c.Name, err)
			return false
		}
	}
	c.Conn = nil
	return true
}

func (c *CommunicationTcpServerTemplate) WriteData(data []byte) int {

	if c.Conn != nil {
		cnt, err := c.Conn.Write(data)
		if err != nil {
			//setting.Logger.Errorf("%s TcpServer write err %v", c.Name, err)
			return 0
		}
		return cnt
	}
	return 0
}

func (c *CommunicationTcpServerTemplate) ReadData(data []byte) int {

	if c.Conn != nil {
		cnt, err := c.Conn.Read(data)
		//setting.Logger.Debugf("%s,TcpServer read data cnt %v", c.Name, cnt)
		if err != nil {
			//setting.Logger.Errorf("%s,TcpServer read err,%v", c.Name, err)
			return 0
		}
		return cnt
	}
	return 0
}

func (c *CommunicationTcpServerTemplate) GetName() string {
	return c.Name
}

func (c *CommunicationTcpServerTemplate) GetTimeOut() string {
	return c.Param.Timeout
}

func (c *CommunicationTcpServerTemplate) GetInterval() string {
	return c.Param.Interval
}

func ReadCommTcpServerInterfaceListFromJson() bool {

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileDir := exeCurDir + "/selfpara/commTcpServerInterface.json"

	if fileExist(fileDir) == true {
		fp, err := os.OpenFile(fileDir, os.O_RDONLY, 0777)
		if err != nil {
			log.Println("open commTcpServerInterface.json err", err)
			return false
		}
		defer fp.Close()

		data := make([]byte, 20480)
		dataCnt, err := fp.Read(data)

		err = json.Unmarshal(data[:dataCnt], &CommunicationTcpServerMap)
		if err != nil {
			setting.Logger.Errorf("commTcpServerInterface unmarshal err", err)
			return false
		}
		return true
	} else {
		setting.Logger.Debugf("commTcpServerInterface.json is not exist")

		return false
	}
}

func WriteCommTcpServerInterfaceListToJson() {

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	fileDir := exeCurDir + "/selfpara/commTcpServerInterface.json"

	fp, err := os.OpenFile(fileDir, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		log.Println("open commTcpServerInterface.json err", err)
		return
	}
	defer fp.Close()

	sJson, _ := json.Marshal(CommunicationTcpServerMap)

	_, err = fp.Write(sJson)
	if err != nil {
		log.Println("write commTcpServerInterface.json err", err)
	}
	setting.Logger.Infof("write commTcpServerInterface.json sucess")
}
