package device

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"goAdapter/device/commInterface"
	"goAdapter/device/eventBus"
	"goAdapter/setting"
	"log"
	"os"
	"path/filepath"
	"sync"
	"time"

	lua "github.com/yuin/gopher-lua"

	"github.com/robfig/cron"
)

type CollectInterfaceEventTemplate struct {
	Topic    string      `json:"Topic"`    //事件主题，online，offline，update
	CollName string      `json:"CollName"` //采集接口名称
	NodeName string      `json:"NodeName"` //设备节点名称
	Content  interface{} `json:"Content"`  //事件内容
}

//采集接口模板
type CollectInterfaceTemplate struct {
	CollInterfaceName   string                               `json:"CollInterfaceName"` //采集接口
	CommInterfaceName   string                               `json:"CommInterfaceName"` //通信接口
	CommInterface       commInterface.CommunicationInterface `json:"-"`
	CommInterfaceUpdate chan bool                            `json:"-"`                   //通信接口更新
	CommQueueManage     *CommunicationManageTemplate         `json:"-"`                   //通信队列
	PollPeriod          int                                  `json:"PollPeriod"`          //采集周期
	OfflinePeriod       int                                  `json:"OfflinePeriod"`       //离线超时周期
	DeviceNodeCnt       int                                  `json:"DeviceNodeCnt"`       //设备数量
	DeviceNodeOnlineCnt int                                  `json:"DeviceNodeOnlineCnt"` //设备在线数量
	DeviceNodeMap       map[string]*DeviceNodeTemplate       `json:"DeviceNodeMap"`       //节点表
	CollEventBus        eventBus.Bus                         `json:"-"`                   //事件总线
	Cron                *cron.Cron                           `json:"-"`                   //定时管理
	ContextCancelFun    context.CancelFunc                   `json:"-"`
	TSLLuaStateMap      map[string]*lua.LState               `json:"-"`
	TSLEventSub         eventBus.Sub                         `json:"-"`
}

type CollectInterfaceMapTemplate struct {
	Lock sync.RWMutex
	Coll map[string]*CollectInterfaceTemplate
}

//var CollectInterfaceMap = make(map[string]*CollectInterfaceTemplate)
var CollectInterfaceMap = CollectInterfaceMapTemplate{
	Lock: sync.RWMutex{},
	Coll: make(map[string]*CollectInterfaceTemplate),
}

func CollectInterfaceInit() {

	//通信接口
	commInterface.CommInterfaceInit()
	//采集接口
	if ReadCollectInterfaceManageFromJson() == true {
		setting.Logger.Debugf("read collectInterface json ok")
		CollectInterfaceMap.Lock.Lock()
		for _, v := range CollectInterfaceMap.Coll {
			//立马进行一次采集，加快设备第一次通信速度
			v.CommunicationManagePoll()
		}
		CollectInterfaceMap.Lock.Unlock()
	} else {

	}
}

func WriteCollectInterfaceManageToJson() {

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	fileDir := exeCurDir + "/selfpara/collInterface.json"

	fp, err := os.OpenFile(fileDir, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		log.Println("open collInterface.json err", err)
		return
	}
	defer fp.Close()

	//采集接口配置参数
	type CollectInterfaceParamTemplate struct {
		CollInterfaceName string   `json:"CollInterfaceName"` //采集接口
		CommInterfaceName string   `json:"CommInterfaceName"` //通信接口
		PollPeriod        int      `json:"PollPeriod"`        //采集周期
		OfflinePeriod     int      `json:"OfflinePeriod"`     //离线超时周期
		DeviceNodeCnt     int      `json:"DeviceNodeCnt"`     //设备数量
		DeviceNodeNameMap []string `json:"DeviceNodeNameMap"` //节点名称
		DeviceNodeAddrMap []string `json:"DeviceNodeAddrMap"` //节点地址
		DeviceNodeTypeMap []string `json:"DeviceNodeTypeMap"` //节点类型

	}

	//定义采集接口参数结构体
	CollectInterfaceParamMap := struct {
		CollectInterfaceParam []CollectInterfaceParamTemplate
	}{
		CollectInterfaceParam: make([]CollectInterfaceParamTemplate, 0),
	}

	for _, v := range CollectInterfaceMap.Coll {
		ParamTemplate := CollectInterfaceParamTemplate{
			CollInterfaceName: v.CollInterfaceName,
			CommInterfaceName: v.CommInterfaceName,
			PollPeriod:        v.PollPeriod,
			OfflinePeriod:     v.OfflinePeriod,
			DeviceNodeCnt:     v.DeviceNodeCnt,
		}

		ParamTemplate.DeviceNodeNameMap = make([]string, 0)
		ParamTemplate.DeviceNodeAddrMap = make([]string, 0)
		ParamTemplate.DeviceNodeTypeMap = make([]string, 0)

		for _, d := range v.DeviceNodeMap {
			ParamTemplate.DeviceNodeNameMap = append(ParamTemplate.DeviceNodeNameMap, d.Name)
			ParamTemplate.DeviceNodeAddrMap = append(ParamTemplate.DeviceNodeAddrMap, d.Addr)
			ParamTemplate.DeviceNodeTypeMap = append(ParamTemplate.DeviceNodeTypeMap, d.Type)
		}

		CollectInterfaceParamMap.CollectInterfaceParam = append(CollectInterfaceParamMap.CollectInterfaceParam,
			ParamTemplate)
	}

	sJson, _ := json.Marshal(CollectInterfaceParamMap)

	_, err = fp.Write(sJson)
	if err != nil {
		setting.Logger.Errorf("write collInterface.json err %v", err)
	}
	setting.Logger.Info("write collInterface.json sucess")
}

func fileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}

func ReadCollectInterfaceManageFromJson() bool {

	exeCurDir := setting.GetExeDir()
	fileDir := exeCurDir + "/selfpara/collInterface.json"

	if fileExist(fileDir) == true {
		fp, err := os.OpenFile(fileDir, os.O_RDONLY, 0777)
		if err != nil {
			log.Println("open collInterface.json err", err)
			return false
		}
		defer fp.Close()

		data := make([]byte, 20480)
		dataCnt, err := fp.Read(data)

		//采集接口配置参数
		type CollectInterfaceParamTemplate struct {
			CollInterfaceName string   `json:"CollInterfaceName"` //采集接口
			CommInterfaceName string   `json:"CommInterfaceName"` //通信接口
			PollPeriod        int      `json:"PollPeriod"`        //采集周期
			OfflinePeriod     int      `json:"OfflinePeriod"`     //离线超时周期
			DeviceNodeCnt     int      `json:"DeviceNodeCnt"`     //设备数量
			DeviceNodeNameMap []string `json:"DeviceNodeNameMap"` //节点名称
			DeviceNodeAddrMap []string `json:"DeviceNodeAddrMap"` //节点地址
			DeviceNodeTypeMap []string `json:"DeviceNodeTypeMap"` //节点类型
		}

		//定义采集接口参数结构体
		CollectInterfaceParamMap := struct {
			CollectInterfaceParam []CollectInterfaceParamTemplate
		}{
			CollectInterfaceParam: make([]CollectInterfaceParamTemplate, 0),
		}

		err = json.Unmarshal(data[:dataCnt], &CollectInterfaceParamMap)
		if err != nil {
			log.Println("collInterface unmarshal err", err)

			return false
		}

		for _, v := range CollectInterfaceParamMap.CollectInterfaceParam {
			//创建接口实例
			CollectInterfaceMap.Coll[v.CollInterfaceName] = NewCollectInterface(v.CollInterfaceName,
				v.CommInterfaceName,
				v.PollPeriod,
				v.OfflinePeriod,
				v.DeviceNodeCnt)

			//创建设备实例
			for i := 0; i < v.DeviceNodeCnt; i++ {
				CollectInterfaceMap.Coll[v.CollInterfaceName].NewDeviceNode(
					v.DeviceNodeNameMap[i],
					v.DeviceNodeTypeMap[i],
					v.DeviceNodeAddrMap[i])
			}
		}

		return true
	} else {
		setting.Logger.Info("collInterface.json is not exist")

		return false
	}
}

/********************************************************
功能描述：	增加接口
参数说明：
返回说明：
调用方式：
全局变量：
读写时间：
注意事项：
日期    ：
********************************************************/
func NewCollectInterface(collInterfaceName, commInterfaceName string,
	pollPeriod, offlinePeriod int, deviceNodeCnt int) *CollectInterfaceTemplate {

	index := -1
	for k, v := range commInterface.CommunicationInterfaceMap {
		if v.GetName() == commInterfaceName {
			index = k
			break
		}
	}
	if index == -1 {
		return nil
	}

	coll := &CollectInterfaceTemplate{
		CollInterfaceName:   collInterfaceName,
		CommInterfaceName:   commInterfaceName,
		CommInterface:       commInterface.CommunicationInterfaceMap[index],
		CommInterfaceUpdate: make(chan bool),
		CommQueueManage:     NewCommunicationManageTemplate(),
		PollPeriod:          pollPeriod,
		OfflinePeriod:       offlinePeriod,
		DeviceNodeCnt:       deviceNodeCnt,
		DeviceNodeMap:       make(map[string]*DeviceNodeTemplate),
		CollEventBus:        eventBus.NewBus(),
		Cron:                cron.New(),
		TSLLuaStateMap:      make(map[string]*lua.LState),
		TSLEventSub:         eventBus.NewSub(),
	}

	//将lua文件加载到虚拟机中,每个采集接口单独一组，多个采集接口就不会冲突
	for _, v := range DeviceTSLMap {
		//订阅物模型的修改和删除事件
		v.Event.Subscribe("modify", coll.TSLEventSub)
		v.Event.Subscribe("delete", coll.TSLEventSub)

		err, lState := v.DeviceTSLOpenPlugin()
		if err != nil {
			continue
		}
		coll.TSLLuaStateMap[v.Name] = lState
	}

	ctx, cancel := context.WithCancel(context.Background())
	coll.ContextCancelFun = cancel

	str := fmt.Sprintf("@every %dm%ds", coll.PollPeriod/60, coll.PollPeriod%60)
	setting.Logger.Infof("str %+v", str)
	//添加定时任务
	_ = coll.Cron.AddFunc(str, coll.CommunicationManagePoll)
	coll.Cron.Start()
	//创建通信接口接收协程
	go coll.CommQueueManage.CommunicationManageProcessReceiveData(ctx, coll.CommInterface)
	//创建通信队列处理协程
	go coll.CommunicationManageDel(ctx)
	//创建通信接口更新协程
	go coll.CommInterfaceProcessUpdate(ctx)
	//创建物模型更新协程
	go coll.TSLProcessUpdate(ctx)
	return coll
}

func AddCollectInterface(collName string, commName string, PollPeriod, OfflinePeriod int) error {

	CollectInterfaceMap.Lock.Lock()
	_, ok := CollectInterfaceMap.Coll[collName]
	CollectInterfaceMap.Lock.Unlock()
	if ok {
		return errors.New("collName is exist")
	}

	CollectInterfaceMap.Coll[collName] = NewCollectInterface(collName, commName, PollPeriod, OfflinePeriod, 0)
	WriteCollectInterfaceManageToJson()

	return nil
}

/********************************************************
功能描述：	修改接口
参数说明：
返回说明：
调用方式：
全局变量：
读写时间：
注意事项：
日期    ：
********************************************************/
func ModifyCollectInterface(collName string, commName string, pollPeriod, offlinePeriod int) error {

	coll, ok := CollectInterfaceMap.Coll[collName]
	if !ok {
		return errors.New("collName is not exist")
	}

	//停止
	coll.Cron.Stop()
	//重启
	coll.Cron = cron.New()
	str := fmt.Sprintf("@every %dm%ds", pollPeriod/60, pollPeriod%60)
	setting.Logger.Infof("str %+v", str)
	//添加定时任务
	_ = coll.Cron.AddFunc(str, coll.CommunicationManagePoll)
	coll.Cron.Start()

	//通信接口发生了变化
	if coll.CommInterfaceName != commName {
		//相关协程退出
		coll.ContextCancelFun()

		//关闭旧的通信接口
		coll.CommInterface.Close()
		index := -1
		for k, v := range commInterface.CommunicationInterfaceMap {
			if v.GetName() == commName {
				index = k
				break
			}
		}
		setting.Logger.Debugf("index %v", index)
		if index == -1 {
			return nil
		}
		coll.CommInterface = commInterface.CommunicationInterfaceMap[index]
		//打开新的通信接口
		coll.CommInterface.Open()

		ctx, cancel := context.WithCancel(context.Background())
		coll.ContextCancelFun = cancel

		//创建通信接口接收协程
		go coll.CommQueueManage.CommunicationManageProcessReceiveData(ctx, coll.CommInterface)
		//创建通信队列处理协程
		go coll.CommunicationManageDel(ctx)
		//创建通信接口更新协程
		go coll.CommInterfaceProcessUpdate(ctx)
		//创建物模型更新协程
		go coll.TSLProcessUpdate(ctx)
	}

	coll.CommInterfaceName = commName
	coll.PollPeriod = pollPeriod
	coll.OfflinePeriod = offlinePeriod
	WriteCollectInterfaceManageToJson()

	return nil
}

/********************************************************
功能描述：	删除接口
参数说明：
返回说明：
调用方式：
全局变量：
读写时间：
注意事项：
日期    ：
********************************************************/
func DeleteCollectInterface(collName string) error {

	CollectInterfaceMap.Lock.Lock()
	coll, ok := CollectInterfaceMap.Coll[collName]
	CollectInterfaceMap.Lock.Unlock()
	if !ok {
		return errors.New("collName is not exist")
	}

	coll.Cron.Stop()

	//相关协程退出
	coll.ContextCancelFun()

	delete(CollectInterfaceMap.Coll, collName)
	WriteCollectInterfaceManageToJson()

	return nil
}

/********************************************************
功能描述：	增加单个节点
参数说明：
返回说明：
调用方式：
全局变量：
读写时间：
注意事项：
日期    ：
********************************************************/
func (d *CollectInterfaceTemplate) NewDeviceNode(dName string, dType string, dAddr string) {

	node := &DeviceNodeTemplate{
		Index:          len(d.DeviceNodeMap),
		Name:           dName,
		Addr:           dAddr,
		Type:           dType,
		LastCommRTC:    "1970-01-01 00:00:00",
		CommTotalCnt:   0,
		CommSuccessCnt: 0,
		CurCommFailCnt: 0,
		CommStatus:     "offLine",
	}

	properties := node.NewVariables()
	node.Properties = append(node.Properties, properties...)
	services := node.NewServices()
	node.Services = append(node.Services, services...)

	d.DeviceNodeMap[dName] = node
}

func (d *CollectInterfaceTemplate) AddDeviceNode(dName string, dType string, dAddr string) (bool, string) {

	node := &DeviceNodeTemplate{}
	node.Index = len(d.DeviceNodeMap)
	node.Name = dName
	node.Addr = dAddr
	node.Type = dType
	node.LastCommRTC = "1970-01-01 00:00:00"
	node.CommTotalCnt = 0
	node.CommSuccessCnt = 0
	node.CurCommFailCnt = 0
	node.CommStatus = "offLine"
	//node.VariableMap = make([]VariableTemplate, 0)
	//variables := node.NewVariables()
	//node.VariableMap = append(node.VariableMap, variables...)

	properties := node.NewVariables()
	node.Properties = append(node.Properties, properties...)
	services := node.NewServices()
	node.Services = append(node.Services, services...)

	d.DeviceNodeMap[dName] = node

	d.DeviceNodeCnt++

	return true, "add success"
}

func (d *CollectInterfaceTemplate) DeleteDeviceNode(dName string) {

	_, ok := d.DeviceNodeMap[dName]
	if ok {
		d.DeviceNodeCnt--
		delete(d.DeviceNodeMap, dName)
	}
}

func (d *CollectInterfaceTemplate) GetDeviceNode(dAddr string) *DeviceNodeTemplate {

	for _, v := range d.DeviceNodeMap {
		if v.Addr == dAddr {
			return v
		}
	}

	return nil
}

func (d *CollectInterfaceTemplate) CommInterfaceProcessUpdate(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			setting.Logger.Debugf("采集接口[%s]处理更新协程退出", d.CollInterfaceName)
			return
		case <-d.CommInterfaceUpdate:
			{
				setting.Logger.Debugf("通信接口[%s]发生更新事件", d.CommInterfaceName)
				rt := d.CommInterface.Open()
				if rt != true {
					setting.Logger.Debugf("通信接口[%s]重新打开失败", d.CommInterfaceName)
				} else {
					setting.Logger.Debugf("通信接口[%s]重新打开成功", d.CommInterfaceName)
				}
				//采集队列协程退出
				d.CommQueueManage.QuitChan <- true
				time.Sleep(100 * time.Millisecond)
				//创建通信接口接收协程
				go d.CommQueueManage.CommunicationManageProcessReceiveData(ctx, d.CommInterface)
			}
		}
	}
}

func (d *CollectInterfaceTemplate) TSLProcessUpdate(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			setting.Logger.Debugf("采集接口[%s]处理物模型更新协程退出", d.CollInterfaceName)
			return
		default:
			{
				msg := d.TSLEventSub.Out().(DeviceTSLEventTemplate)
				nodeName := ""
				for k, v := range d.DeviceNodeMap {
					if v.Type == msg.Type {
						nodeName = k
					}
				}
				if nodeName == "" {
					continue
				}
				setting.Logger.Debugf("采集接口[%s] 物模型[%s] 发生事件[%s]", d.CollInterfaceName, msg.Type, msg.Topic)
				switch msg.Topic {
				case "modify":
					{
						d.DeviceNodeMap[nodeName].Properties = d.DeviceNodeMap[nodeName].Properties[0:0]
						properties := d.DeviceNodeMap[nodeName].NewVariables()
						d.DeviceNodeMap[nodeName].Properties = append(d.DeviceNodeMap[nodeName].Properties, properties...)
						d.DeviceNodeMap[nodeName].Services = d.DeviceNodeMap[nodeName].Services[0:0]
						d.DeviceNodeMap[nodeName].NewServices()
					}
				case "delete":
					{
						d.DeviceNodeMap[nodeName].Properties = d.DeviceNodeMap[nodeName].Properties[0:0]
						d.DeviceNodeMap[nodeName].Services = d.DeviceNodeMap[nodeName].Services[0:0]
					}
				}
				time.Sleep(time.Second)
			}
		}
	}
}

func (d *CollectInterfaceTemplate) CommunicationManageDel(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			setting.Logger.Debugf("%s exit", d.CollInterfaceName)
			return
		case cmd := <-d.CommQueueManage.EmergencyRequestChan:
			{
				setting.Logger.Infof("emergency chan collName %v nodeName %v funName %v", d.CollInterfaceName, cmd.DeviceName, cmd.FunName)

				node, ok := d.DeviceNodeMap[cmd.DeviceName]
				if !ok {
					continue
				}
				rxData := d.CommQueueManage.CommunicationStateMachine(cmd,
					d.CollInterfaceName,
					d.CommInterface,
					node,
					&d.CollEventBus,
					d.TSLLuaStateMap,
					d.OfflinePeriod)
				//更新设备在线数量
				d.DeviceNodeOnlineCnt = 0
				for _, v := range d.DeviceNodeMap {
					if v.CommStatus == "onLine" {
						d.DeviceNodeOnlineCnt++
					}
				}

				setting.Logger.Debugf("emergency chan rxData %v", rxData)
				d.CommQueueManage.EmergencyAckChan <- rxData
			}
		default:
			{
				select {
				case req := <-d.CommQueueManage.DirectDataRequestChan:
					{
						ack := d.CommQueueManage.CommunicationDirectDataStateMachine(req, d.CommInterface)
						d.CommQueueManage.DirectDataAckChan <- ack
					}
				case cmd := <-d.CommQueueManage.CommonRequestChan:
					{
						node, ok := d.DeviceNodeMap[cmd.DeviceName]
						if !ok {
							continue
						}

						setting.Logger.Debugf("%v:commChanLen %v", d.CollInterfaceName, len(d.CommQueueManage.CommonRequestChan))
						d.CommQueueManage.CommunicationStateMachine(cmd,
							d.CollInterfaceName,
							d.CommInterface,
							node,
							&d.CollEventBus,
							d.TSLLuaStateMap,
							d.OfflinePeriod)
						//更新设备在线数量,当本次采集最后一个设备时进行更新
						if len(d.CommQueueManage.CommonRequestChan) == 0 {
							d.DeviceNodeOnlineCnt = 0
							for _, v := range d.DeviceNodeMap {
								if v.CommStatus == "onLine" {
									d.DeviceNodeOnlineCnt++
								}
							}
						}
					}
				default:
					time.Sleep(100 * time.Millisecond)
				}
			}
		}
	}
}

func (d *CollectInterfaceTemplate) CommunicationManagePoll() {

	cmd := CommunicationCmdTemplate{}
	for _, v := range d.DeviceNodeMap {
		cmd.CollInterfaceName = d.CollInterfaceName
		cmd.DeviceName = v.Name
		cmd.FunName = "GetDeviceRealVariables"
		d.CommQueueManage.CommunicationManageAddCommon(cmd)
	}
	setting.Logger.Debugf("%s commChanTotalLen %v", d.CollInterfaceName, len(d.CommQueueManage.CommonRequestChan))
}
