package device

import (
	"encoding/json"
	"errors"
	"goAdapter/device/eventBus"
	"goAdapter/setting"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	lua "github.com/yuin/gopher-lua"
)

const (
	TSLAccessModeRead int = iota
	TSLAccessModeWrite
	TSLAccessModeReadWrite
)

const (
	PropertyTypeUInt32 int = iota
	PropertyTypeInt32
	PropertyTypeDouble
	PropertyTypeString
)

type DeviceTSLPropertyParamTempate struct {
	Min             string `json:"Min,omitempty"`             //最小
	Max             string `json:"Max,omitempty"`             //最大
	MinMaxAlarm     bool   `json:"MinMaxAlarm,omitempty"`     //范围报警
	Step            string `json:"Step,omitempty"`            //步长
	StepAlarm       bool   `json:"StepAlarm,omitempty"`       //阶跃报警
	Decimals        string `json:"Decimals,omitempty"`        //小数位数
	DataLength      string `json:"DataLength,omitempty"`      //字符串长度
	DataLengthAlarm bool   `json:"DataLengthAlarm,omitempty"` //字符长度报警
	Unit            string `json:"Unit"`                      //单位
}

type DeviceTSLPropertyValueTemplate struct {
	Index     int         `json:"Index"`
	Value     interface{} `json:"Value"`   //变量值，不可以是字符串
	Explain   interface{} `json:"Explain"` //变量值解释，必须是字符串
	TimeStamp string      `json:"TimeStamp"`
}

type DeviceTSLPropertyTemplate struct {
	Name       string                           `json:"Name"`       //属性名称，只可以是字母和数字的组合
	Explain    string                           `json:"Explain"`    //属性解释
	AccessMode int                              `json:"AccessMode"` //读写属性
	Type       int                              `json:"Type"`       //类型 uint32 int32 double string
	Params     DeviceTSLPropertyParamTempate    `json:"Params"`
	Value      []DeviceTSLPropertyValueTemplate `json:"Value"`
}

type DeviceTSLServiceTempalte struct {
	Name     string                 `json:"Name"`     //服务名称
	Explain  string                 `json:"Explain"`  //服务名称说明
	CallType int                    `json:"CallType"` //服务调用方式
	Params   map[string]interface{} `json:"Params"`   //服务参数
}

//物模型 Thing Specification Language
type DeviceTSLTemplate struct {
	Name       string                      `json:"Name"`    //名称，只可以是字母和数字的组合
	Explain    string                      `json:"Explain"` //名称解释
	Plugin     string                      `json:"Plugin"`
	Properties []DeviceTSLPropertyTemplate `json:"Properties"` //属性
	Services   []DeviceTSLServiceTempalte  `json:"Services"`   //服务
	Event      eventBus.Bus                `json:"-"`          //事件队列
}

type DeviceTSLEventTemplate struct {
	Topic string
	Type  string
}

var DeviceTSLMap = make([]*DeviceTSLTemplate, 0)

func DeviceTSLInit() {

	ReadDeviceTSLParamFromJson()
}

func ReadDeviceTSLParamFromJson() bool {
	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileDir := exeCurDir + "/selfpara/deviceTSLParam.json"

	if fileExist(fileDir) == true {
		fp, err := os.OpenFile(fileDir, os.O_RDONLY, 0777)
		if err != nil {
			log.Println("open deviceTSLParam.json err,", err)
			return false
		}
		defer fp.Close()

		data := make([]byte, 20480)
		dataCnt, err := fp.Read(data)

		err = json.Unmarshal(data[:dataCnt], &DeviceTSLMap)
		if err != nil {
			setting.Logger.Errorf("deviceTSLParam unmarshal err %v", err)
			return false
		}
		for _, v := range DeviceTSLMap {
			v.Event = eventBus.NewBus()
		}

		setting.Logger.Info("read deviceTSLParam.json ok")
		return true
	} else {
		setting.Logger.Warn("deviceTSLParam.json is not exist")
		return false
	}
}

func WriteDeviceTSLParamToJson() {
	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	fileDir := exeCurDir + "/selfpara/deviceTSLParam.json"

	fp, err := os.OpenFile(fileDir, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		log.Println("open deviceTSLParam.json err", err)
		return
	}
	defer fp.Close()

	sJson, _ := json.Marshal(DeviceTSLMap)

	_, err = fp.Write(sJson)
	if err != nil {
		setting.Logger.Errorf("write deviceTSLParam.json err", err)
	}
	setting.Logger.Debugf("write deviceTSLParam.json success")
}

func NewDeviceTSL(tslName string, tslExplain string) *DeviceTSLTemplate {
	return &DeviceTSLTemplate{
		Name:       tslName,
		Explain:    tslExplain,
		Plugin:     "",
		Properties: make([]DeviceTSLPropertyTemplate, 0),
		Services:   make([]DeviceTSLServiceTempalte, 0),
		Event:      eventBus.NewBus(),
	}
}

func DeviceTSLAdd(tsl *DeviceTSLTemplate) (int, error) {

	for _, v := range DeviceTSLMap {
		if v.Name == tsl.Name {
			return 1, errors.New("tslName is exist")
		}
	}

	DeviceTSLMap = append(DeviceTSLMap, tsl)

	WriteDeviceTSLParamToJson()
	return 0, nil
}

func DeviceTSLDelete(tslName string) (int, error) {

	for k := 0; k < len(DeviceTSLMap); k++ {
		if DeviceTSLMap[k].Name == tslName {
			DeviceTSLMap = append(DeviceTSLMap[:k], DeviceTSLMap[k+1:]...)
			WriteDeviceTSLParamToJson()
			return 0, nil
		}
	}

	return 1, errors.New("tslName is not exist")
}

func DeviceTSLModifyExplain(name string, explain string) (int, error) {

	index := -1
	for k := 0; k < len(DeviceTSLMap); k++ {
		if DeviceTSLMap[k].Name == name {
			index = k
			break
		}
	}

	if index == -1 {
		return 1, errors.New("tslName is not exist")
	}

	DeviceTSLMap[index].Explain = explain
	WriteDeviceTSLParamToJson()
	return 0, nil
}

func DeviceTSLModifyPlugin(name string, plugin string) (int, error) {

	index := -1
	for k := 0; k < len(DeviceTSLMap); k++ {
		if DeviceTSLMap[k].Name == name {
			index = k
			break
		}
	}

	if index == -1 {
		return 1, errors.New("tslName is not exist")
	}

	DeviceTSLMap[index].Plugin = plugin
	WriteDeviceTSLParamToJson()
	return 0, nil
}

//遍历plugin
func DeviceTSLTraversePlugin(path string, fileName []string) ([]string, error) {

	rd, err := ioutil.ReadDir(path)
	if err != nil {
		log.Println("readDir err,", err)
		return fileName, err
	}

	for _, fi := range rd {
		if fi.IsDir() {
			fullDir := path + "/" + fi.Name()
			fileName, _ = DeviceTSLTraversePlugin(fullDir, fileName)
		} else {
			fullName := path + "/" + fi.Name()
			if strings.Contains(fi.Name(), ".json") {
				fileName = append(fileName, fullName)
			} else if strings.Contains(fi.Name(), ".lua") {
				fileName = append(fileName, fullName)
			}
		}
	}

	return fileName, nil
}

func DeviceTSLExportPlugin(pluginName string) (bool, string) {

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	//遍历文件
	pluginPath := exeCurDir + "/plugin/" + pluginName
	fileNameMap := make([]string, 0)
	fileNameMap, _ = DeviceTSLTraversePlugin(pluginPath, fileNameMap)

	//保留原来文件的结构
	err := setting.ZipFiles(exeCurDir+"/plugin/"+pluginName+".zip", fileNameMap, exeCurDir+"/plugin", "plugin/")
	if err != nil {
		setting.Logger.Errorf("zipFile err %v", err)
		return false, ""
	}

	return true, exeCurDir + "/plugin/" + pluginName + ".zip"
}

func (d *DeviceTSLTemplate) DeviceTSLOpenPlugin() (error, *lua.LState) {

	lState := &lua.LState{}

	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	//遍历json和lua文件
	pluginPath := exeCurDir + "/plugin"
	fileInfoMap, err := ioutil.ReadDir(pluginPath)
	if err != nil {
		setting.Logger.Errorf("read plugin err %v", err)
		return err, nil
	}
	for _, v := range fileInfoMap {
		//文件夹并且文件名字和物模型plugin相同
		if (v.IsDir() == true) || (v.Name() == d.Plugin) {
			fileDirName := pluginPath + "/" + v.Name()
			fileMap, _ := ioutil.ReadDir(fileDirName)

			index := -1
			for k, f := range fileMap {

				fileFullName := fileDirName + "/" + f.Name()
				if strings.Contains(f.Name(), ".lua") {
					//lua文件和设备模版名字一样
					if strings.EqualFold(f.Name(), d.Plugin+".lua") == true {
						index = k
						lState, err = setting.LuaOpenFile(fileFullName)
						if err != nil {
							setting.Logger.Errorf("openPlug %s err %v", f.Name(), err)
							continue
						} else {
							setting.Logger.Debugf("openPlug %s ok", f.Name())
						}
						lState.SetGlobal("GetCRCModbus", lState.NewFunction(setting.GetCRCModbus))
						lState.SetGlobal("CheckCRCModbus", lState.NewFunction(setting.CheckCRCModbus))
						lState.SetGlobal("GetCRCModbusLittleEndian", lState.NewFunction(setting.GetCRCModbusLittleEndian))
						break
					}
				}
			}
			if index == -1 {
				continue
			}

			for _, f := range fileMap {
				fileFullName := fileDirName + "/" + f.Name()
				if strings.Contains(f.Name(), ".lua") {
					//lua文件和设备模版名字不一样
					if strings.Contains(f.Name(), d.Plugin) == false {
						err = lState.DoFile(fileFullName)
						if err != nil {
							setting.Logger.Errorf("DoFile %s err %v", f.Name(), err)
						} else {
							setting.Logger.Debugf("DoFile %s  ok", f.Name())
						}
					}
				}
			}
		}
	}

	return nil, lState
}

func (d *DeviceTSLTemplate) DeviceTSLPropertiesAdd(property DeviceTSLPropertyTemplate) (int, error) {

	properties := make([]DeviceTSLPropertyTemplate, 0)
	for _, v := range d.Properties {
		v.Value = make([]DeviceTSLPropertyValueTemplate, 0)
		properties = append(properties, v)
	}
	for _, v := range properties {
		if v.Name == property.Name {
			return 1, errors.New("property is exist")
		}
	}

	properties = append(properties, property)

	d.Properties = properties
	WriteDeviceTSLParamToJson()
	return 0, nil
}

func (d *DeviceTSLTemplate) DeviceTSLPropertiesDelete(propertiesName []string) (int, error) {

	cnt := 0
	for _, p := range propertiesName {
		for k := 0; k < len(d.Properties); k++ {
			if d.Properties[k].Name == p {
				setting.Logger.Debugf("Properties index %v", k)
				cnt = 1
				d.Properties = append(d.Properties[:k], d.Properties[k+1:]...)
				setting.Logger.Debugf("Properties %v", d.Properties)
			}
		}
	}
	if cnt != 0 {
		WriteDeviceTSLParamToJson()
		return 0, nil
	}
	return 1, errors.New("property is not exist")

}

func (d *DeviceTSLTemplate) DeviceTSLPropertiesModify(property *DeviceTSLPropertyTemplate) (int, error) {

	properties := make([]DeviceTSLPropertyTemplate, 0)
	for _, v := range d.Properties {
		properties = append(properties, v)
	}
	index := -1
	for k, v := range properties {
		if v.Name == property.Name {
			properties[k] = *property
			index = k
		}
	}
	if index != -1 {
		d.Properties = properties
		WriteDeviceTSLParamToJson()
		return 0, nil
	}

	return 1, errors.New("property is not exist")
}

func (d *DeviceTSLTemplate) DeviceTSLServicesAdd(service DeviceTSLServiceTempalte) (int, error) {

	services := make([]DeviceTSLServiceTempalte, 0)
	for _, v := range d.Services {
		v.Params = make(map[string]interface{})
		services = append(services, v)
	}
	for _, v := range services {
		if v.Name == service.Name {
			return 1, errors.New("service is exist")
		}
	}

	services = append(services, service)

	d.Services = services
	WriteDeviceTSLParamToJson()
	return 0, nil
}

func (d *DeviceTSLTemplate) DeviceTSLServicesDelete(servicesName []string) (int, error) {

	cnt := 0
	for _, p := range servicesName {
		for k := 0; k < len(d.Services); k++ {
			if d.Services[k].Name == p {
				setting.Logger.Debugf("Services index %v", k)
				cnt = 1
				d.Services = append(d.Services[:k], d.Services[k+1:]...)
				setting.Logger.Debugf("Services %v", d.Services)
			}
		}
	}
	if cnt != 0 {
		WriteDeviceTSLParamToJson()
		return 0, nil
	}
	return 1, errors.New("servicesName is not exist")

}

func (d *DeviceTSLTemplate) DeviceTSLServicesModify(service *DeviceTSLServiceTempalte) (int, error) {

	for k, v := range d.Services {
		if v.Name == service.Name {
			d.Services[k] = *service
			WriteDeviceTSLParamToJson()
			return 0, nil
		}
	}
	return 1, errors.New("service is not exist")
}
