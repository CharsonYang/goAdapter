package mqttEmqx

import (
	"encoding/json"
	"fmt"
	"goAdapter/device"
	"goAdapter/device/eventBus"
	"goAdapter/setting"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/robfig/cron"
)

//上报节点参数结构体
type ReportServiceNodeParamEmqxTemplate struct {
	ServiceName       string
	CollInterfaceName string
	Name              string
	Addr              string
	CommStatus        string
	ReportErrCnt      int `json:"-"`
	ReportStatus      string
	Protocol          string
	Param             struct {
		ClientID string
	}
}

//上报网关参数结构体
type ReportServiceGWParamEmqxTemplate struct {
	ServiceName  string
	IP           string
	Port         string
	ReportStatus string
	ReportTime   int
	ReportErrCnt int
	Protocol     string
	Param        struct {
		UserName string
		Password string
		ClientID string
	}
	MQTTClient MQTT.Client `json:"-"`
}

//上报服务参数，网关参数，节点参数
type ReportServiceParamEmqxTemplate struct {
	GWParam                              ReportServiceGWParamEmqxTemplate
	NodeList                             []ReportServiceNodeParamEmqxTemplate
	ReceiveFrameChan                     chan MQTTEmqxReceiveFrameTemplate         `json:"-"`
	LogInRequestFrameChan                chan []string                             `json:"-"` //上线
	ReceiveLogInAckFrameChan             chan MQTTEmqxLogInAckTemplate             `json:"-"`
	LogOutRequestFrameChan               chan []string                             `json:"-"`
	ReceiveLogOutAckFrameChan            chan MQTTEmqxLogOutAckTemplate            `json:"-"`
	ReportPropertyRequestFrameChan       chan MQTTEmqxReportPropertyTemplate       `json:"-"`
	ReceiveReportPropertyAckFrameChan    chan MQTTEmqxReportPropertyAckTemplate    `json:"-"`
	ReceiveInvokeServiceRequestFrameChan chan MQTTEmqxInvokeServiceRequestTemplate `json:"-"`
	ReceiveInvokeServiceAckFrameChan     chan MQTTEmqxInvokeServiceAckTemplate     `json:"-"`
	ReceiveWritePropertyRequestFrameChan chan MQTTEmqxWritePropertyRequestTemplate `json:"-"`
	ReceiveReadPropertyRequestFrameChan  chan MQTTEmqxReadPropertyRequestTemplate  `json:"-"`
}

type ReportServiceParamListEmqxTemplate struct {
	ServiceList []*ReportServiceParamEmqxTemplate
}

//实例化上报服务
var ReportServiceParamListEmqx = &ReportServiceParamListEmqxTemplate{
	ServiceList: make([]*ReportServiceParamEmqxTemplate, 0),
}

func ReportServiceEmqxInit() {

	ReportServiceParamListEmqx.ReadParamFromJson()

	//初始化
	for _, v := range ReportServiceParamListEmqx.ServiceList {
		v.ReceiveFrameChan = make(chan MQTTEmqxReceiveFrameTemplate, 100)
		v.LogInRequestFrameChan = make(chan []string, 0)
		v.ReceiveLogInAckFrameChan = make(chan MQTTEmqxLogInAckTemplate, 5)
		v.LogOutRequestFrameChan = make(chan []string, 0)
		v.ReceiveLogOutAckFrameChan = make(chan MQTTEmqxLogOutAckTemplate, 5)
		v.ReportPropertyRequestFrameChan = make(chan MQTTEmqxReportPropertyTemplate, 50)
		v.ReceiveReportPropertyAckFrameChan = make(chan MQTTEmqxReportPropertyAckTemplate, 50)
		v.ReceiveInvokeServiceRequestFrameChan = make(chan MQTTEmqxInvokeServiceRequestTemplate, 50)
		v.ReceiveInvokeServiceAckFrameChan = make(chan MQTTEmqxInvokeServiceAckTemplate, 50)
		v.ReceiveWritePropertyRequestFrameChan = make(chan MQTTEmqxWritePropertyRequestTemplate, 50)
		v.ReceiveReadPropertyRequestFrameChan = make(chan MQTTEmqxReadPropertyRequestTemplate, 50)

		go ReportServiceEmqxPoll(v)
	}
}

func fileExist(path string) bool {
	_, err := os.Lstat(path)
	return !os.IsNotExist(err)
}

func (s *ReportServiceParamListEmqxTemplate) ReadParamFromJson() bool {
	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	fileDir := exeCurDir + "/selfpara/reportServiceParamListEmqx.json"

	if fileExist(fileDir) == true {
		fp, err := os.OpenFile(fileDir, os.O_RDONLY, 0777)
		if err != nil {
			log.Println("open reportServiceParamListEmqx.json err,", err)
			return false
		}
		defer fp.Close()

		data := make([]byte, 20480)
		dataCnt, err := fp.Read(data)

		err = json.Unmarshal(data[:dataCnt], s)
		if err != nil {
			log.Println("reportServiceParamListEmqx unmarshal err", err)
			return false
		}
		setting.Logger.Info("read reportServiceParamListEmqx.json ok")
		return true
	} else {
		setting.Logger.Warn("reportServiceParamListEmqx.json is not exist")
		return false
	}
}

func (s *ReportServiceParamListEmqxTemplate) WriteParamToJson() {
	exeCurDir, _ := filepath.Abs(filepath.Dir(os.Args[0]))

	fileDir := exeCurDir + "/selfpara/reportServiceParamListEmqx.json"

	fp, err := os.OpenFile(fileDir, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		log.Println("open reportServiceParamListEmqx.json err", err)
		return
	}
	defer fp.Close()

	sJson, _ := json.Marshal(*s)

	_, err = fp.Write(sJson)
	if err != nil {
		setting.Logger.Errorf("write reportServiceParamListEmqx.json err", err)
	}
	setting.Logger.Debugf("write reportServiceParamListEmqx.json success")
}

func (s *ReportServiceParamListEmqxTemplate) AddReportService(param ReportServiceGWParamEmqxTemplate) {

	for k, v := range s.ServiceList {
		//存在相同的，表示修改;不存在表示增加
		if v.GWParam.ServiceName == param.ServiceName {

			s.ServiceList[k].GWParam = param
			s.WriteParamToJson()
			return
		}
	}

	ReportServiceParam := &ReportServiceParamEmqxTemplate{
		GWParam: param,
	}
	s.ServiceList = append(s.ServiceList, ReportServiceParam)

	s.WriteParamToJson()
}

func (s *ReportServiceParamListEmqxTemplate) DeleteReportService(serviceName string) {

	for k, v := range s.ServiceList {
		if v.GWParam.ServiceName == serviceName {

			s.ServiceList = append(s.ServiceList[:k], s.ServiceList[k+1:]...)
			s.WriteParamToJson()
			return
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) AddReportNode(param ReportServiceNodeParamEmqxTemplate) {

	param.CommStatus = "offLine"
	param.ReportStatus = "offLine"
	param.ReportErrCnt = 0

	//节点存在则进行修改
	for k, v := range r.NodeList {
		//节点已经存在
		if v.Name == param.Name {
			r.NodeList[k] = param
			ReportServiceParamListEmqx.WriteParamToJson()
			return
		}
	}

	//节点不存在则新建
	r.NodeList = append(r.NodeList, param)
	ReportServiceParamListEmqx.WriteParamToJson()

	setting.Logger.Debugf("param %v", ReportServiceParamListEmqx)
}

func (r *ReportServiceParamEmqxTemplate) DeleteReportNode(name string) int {

	index := -1
	//节点存在则进行修改
	for k, v := range r.NodeList {
		//节点已经存在
		if v.Name == name {
			index = k
			r.NodeList = append(r.NodeList[:k], r.NodeList[k+1:]...)
			ReportServiceParamListEmqx.WriteParamToJson()
			return index
		}
	}
	return index
}

func (r *ReportServiceParamEmqxTemplate) ProcessUpLinkFrame() {

	for {
		select {
		case reqFrame := <-r.LogInRequestFrameChan:
			{
				r.LogIn(reqFrame)
			}
		case reqFrame := <-r.LogOutRequestFrameChan:
			{
				r.LogOut(reqFrame)
			}
		case reqFrame := <-r.ReportPropertyRequestFrameChan:
			{
				if reqFrame.DeviceType == "gw" {
					r.GWPropertyPost()
				} else if reqFrame.DeviceType == "node" {
					r.NodePropertyPost(reqFrame.DeviceName)
				}
			}
		case reqFrame := <-r.ReceiveWritePropertyRequestFrameChan:
			{
				r.ReportServiceEmqxProcessWriteProperty(reqFrame)
			}
		case reqFrame := <-r.ReceiveReadPropertyRequestFrameChan:
			{
				r.ReportServiceEmqxProcessReadProperty(reqFrame)
			}
		case reqFrame := <-r.ReceiveInvokeServiceRequestFrameChan:
			{
				r.ReportServiceEmqxProcessInvokeService(reqFrame)
			}
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) ProcessDownLinkFrame() {

	for {
		select {
		case frame := <-r.ReceiveFrameChan:
			{
				//setting.Logger.Debugf("Recv TOPIC: %s", frame.Topic)
				//setting.Logger.Debugf("Recv MSG: %v", frame.Payload)

				if strings.Contains(frame.Topic, "/sys/thing/event/property/post_reply") { //网关、子设备上报属性回应

					ackFrame := MQTTEmqxReportPropertyAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.Logger.Errorf("ReportPropertyAck json unmarshal err")
						return
					}
					r.ReceiveReportPropertyAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/login/post_reply") { //子设备上线回应

					ackFrame := MQTTEmqxLogInAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.Logger.Warningf("LogInAck json unmarshal err")
						return
					}
					r.ReceiveLogInAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/logout/post_reply") { //子设备下线回应

					ackFrame := MQTTEmqxLogOutAckTemplate{}
					err := json.Unmarshal(frame.Payload, &ackFrame)
					if err != nil {
						setting.Logger.Errorf("LogOutAck json unmarshal err")
						return
					}
					r.ReceiveLogOutAckFrameChan <- ackFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/service/invoke") { //设备服务调用
					serviceFrame := MQTTEmqxInvokeServiceRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &serviceFrame)
					if err != nil {
						setting.Logger.Errorf("serviceFrame json unmarshal err")
						return
					}
					r.ReceiveInvokeServiceRequestFrameChan <- serviceFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/property/set") { //设置属性请求
					writePropertyFrame := MQTTEmqxWritePropertyRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &writePropertyFrame)
					if err != nil {
						setting.Logger.Errorf("writePropertyFrame json unmarshal err")
						return
					}
					r.ReceiveWritePropertyRequestFrameChan <- writePropertyFrame
				} else if strings.Contains(frame.Topic, "/sys/thing/event/property/get") { //获取属性请求
					readPropertyFrame := MQTTEmqxReadPropertyRequestTemplate{}
					err := json.Unmarshal(frame.Payload, &readPropertyFrame)
					if err != nil {
						setting.Logger.Errorf("readPropertyFrame json unmarshal err")
						return
					}
					r.ReceiveReadPropertyRequestFrameChan <- readPropertyFrame
				}

			}
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) ProcessCollEvent(sub eventBus.Sub) {
	for {
		msg := sub.Out().(device.CollectInterfaceEventTemplate)
		//判断设备在该上报服务中
		index := -1
		for k, v := range r.NodeList {
			if v.Name == msg.NodeName {
				index = k
			}
		}
		if index == -1 {
			continue
		}
		setting.Logger.Debugf("上报服务[%s] 采集接口[%s] 设备[%s] 主题[%s] 消息内容 %v",
			r.GWParam.ServiceName,
			msg.CollName,
			msg.NodeName,
			msg.Topic,
			msg.Content)
		nodeName := make([]string, 0)
		switch msg.Topic {
		case "onLine":
			{
				nodeName = append(nodeName, msg.NodeName)
				r.NodeList[index].CommStatus = "onLine"
				r.LogInRequestFrameChan <- nodeName
			}
		case "offLine":
			{
				nodeName = append(nodeName, msg.NodeName)
				r.NodeList[index].CommStatus = "offLine"
				r.LogOutRequestFrameChan <- nodeName
			}
		case "update":
			{
				//更新设备的通信状态
				r.NodeList[index].CommStatus = "onLine"

				coll, ok := device.CollectInterfaceMap.Coll[msg.CollName]
				if !ok {
					continue
				}
				node, ok := coll.DeviceNodeMap[msg.NodeName]
				if !ok {
					return
				}

				reportStatus := false
				for _, v := range node.Properties {
					if v.Params.StepAlarm == true {
						valueCnt := len(v.Value)
						if valueCnt >= 2 { //阶跃报警必须是2个值
							if v.Type == device.PropertyTypeInt32 {
								pValueCur := v.Value[valueCnt-1].Value.(int32)
								pValuePre := v.Value[valueCnt-2].Value.(int32)
								step, _ := strconv.Atoi(v.Params.Step)
								if math.Abs(float64(pValueCur-pValuePre)) > float64(step) {
									reportStatus = true //满足报警条件，上报
									nodeName = append(nodeName, node.Name)
								}
							} else if v.Type == device.PropertyTypeUInt32 {
								pValueCur := v.Value[valueCnt-1].Value.(uint32)
								pValuePre := v.Value[valueCnt-2].Value.(uint32)
								step, _ := strconv.Atoi(v.Params.Step)
								if math.Abs(float64(pValueCur-pValuePre)) > float64(step) {
									reportStatus = true //满足报警条件，上报
									nodeName = append(nodeName, node.Name)
								}
							} else if v.Type == device.PropertyTypeDouble {
								pValueCur := v.Value[valueCnt-1].Value.(float64)
								pValuePre := v.Value[valueCnt-2].Value.(float64)
								step, err := strconv.ParseFloat(v.Params.Step, 64)
								if err != nil {
									continue
								}
								if math.Abs(pValueCur-pValuePre) > float64(step) {
									reportStatus = true //满足报警条件，上报
									nodeName = append(nodeName, node.Name)
								}
							}
						}
					} else if v.Params.MinMaxAlarm == true {
						valueCnt := len(v.Value)
						if v.Type == device.PropertyTypeInt32 {
							pValueCur := v.Value[valueCnt-1].Value.(int32)
							min, _ := strconv.Atoi(v.Params.Min)
							max, _ := strconv.Atoi(v.Params.Max)
							if pValueCur < int32(min) || pValueCur > int32(max) {
								reportStatus = true //满足报警条件，上报
								nodeName = append(nodeName, node.Name)
							}
						} else if v.Type == device.PropertyTypeUInt32 {
							pValueCur := v.Value[valueCnt-1].Value.(uint32)
							min, _ := strconv.Atoi(v.Params.Min)
							max, _ := strconv.Atoi(v.Params.Max)
							if pValueCur < uint32(min) || pValueCur > uint32(max) {
								reportStatus = true //满足报警条件，上报
								nodeName = append(nodeName, node.Name)
							}
						} else if v.Type == device.PropertyTypeDouble {
							pValueCur := v.Value[valueCnt-1].Value.(float64)
							min, err := strconv.ParseFloat(v.Params.Min, 64)
							if err != nil {
								continue
							}
							max, err := strconv.ParseFloat(v.Params.Max, 64)
							if err != nil {
								continue
							}
							if pValueCur < min || pValueCur > max {
								reportStatus = true //满足报警条件，上报
								nodeName = append(nodeName, node.Name)
							}
						}
					}
				}

				if reportStatus == true {
					reportNodeProperty := MQTTEmqxReportPropertyTemplate{
						DeviceType: "node",
						DeviceName: nodeName,
					}
					r.ReportPropertyRequestFrameChan <- reportNodeProperty
				}
			}
		}
	}
}

func (r *ReportServiceParamEmqxTemplate) LogIn(nodeName []string) {

	//清空接收chan，避免出现有上次接收的缓存
	for i := 0; i < len(r.ReceiveLogInAckFrameChan); i++ {
		<-r.ReceiveLogInAckFrameChan
	}

	r.NodeLogIn(nodeName)
}

func (r *ReportServiceParamEmqxTemplate) LogOut(nodeName []string) {

	//清空接收chan，避免出现有上次接收的缓存
	for i := 0; i < len(r.ReceiveLogOutAckFrameChan); i++ {
		<-r.ReceiveLogOutAckFrameChan
	}

	r.NodeLogOut(nodeName)
}

func (r *ReportServiceParamEmqxTemplate) ReportTimeFun() {

	if r.GWParam.ReportStatus == "onLine" {
		//网关上报
		reportGWProperty := MQTTEmqxReportPropertyTemplate{
			DeviceType: "gw",
		}
		r.ReportPropertyRequestFrameChan <- reportGWProperty

		//全部末端设备上报
		nodeName := make([]string, 0)
		for _, v := range r.NodeList {
			if v.CommStatus == "onLine" {
				nodeName = append(nodeName, v.Name)
			}
		}
		setting.Logger.Debugf("report Nodes %v", nodeName)
		if len(nodeName) > 0 {
			reportNodeProperty := MQTTEmqxReportPropertyTemplate{
				DeviceType: "node",
				DeviceName: nodeName,
			}
			r.ReportPropertyRequestFrameChan <- reportNodeProperty
		}
	}
}

//查看上报服务中设备是否离线
func (r *ReportServiceParamEmqxTemplate) ReportOfflineTimeFun() {

	setting.Logger.Infof("service:%s CheckReportOffline", r.GWParam.ServiceName)
	//if r.GWParam.ReportErrCnt >= 3 {
	//	r.GWParam.ReportStatus = "offLine"
	//	r.GWParam.ReportErrCnt = 0
	//	setting.Logger.Warningf("service:%s gw offline", r.GWParam.ServiceName)
	//}
	//
	//for k, v := range r.NodeList {
	//	if v.ReportErrCnt >= 3 {
	//		r.NodeList[k].ReportStatus = "offLine"
	//		r.NodeList[k].ReportErrCnt = 0
	//		setting.Logger.Warningf("service:%s %s offline", v.ServiceName, v.Name)
	//	}
	//}
}

func ReportServiceEmqxPoll(r *ReportServiceParamEmqxTemplate) {

	reportState := 0

	// 定义一个cron运行器
	cronProcess := cron.New()

	reportTime := fmt.Sprintf("@every %dm%ds", r.GWParam.ReportTime/60, r.GWParam.ReportTime%60)
	setting.Logger.Infof("reportServiceEmqx reportTime%v", reportTime)

	reportOfflineTime := fmt.Sprintf("@every %dm%ds", (3*r.GWParam.ReportTime)/60, (3*r.GWParam.ReportTime)%60)
	setting.Logger.Infof("reportServiceEmqx reportOfflineTime%v", reportOfflineTime)

	_ = cronProcess.AddFunc(reportOfflineTime, r.ReportOfflineTimeFun)
	_ = cronProcess.AddFunc(reportTime, r.ReportTimeFun)

	//订阅采集接口消息
	device.CollectInterfaceMap.Lock.Lock()
	for _, coll := range device.CollectInterfaceMap.Coll {
		sub := eventBus.NewSub()
		coll.CollEventBus.Subscribe("onLine", sub)
		coll.CollEventBus.Subscribe("offLine", sub)
		coll.CollEventBus.Subscribe("update", sub)
		go r.ProcessCollEvent(sub)
	}
	device.CollectInterfaceMap.Lock.Unlock()

	go r.ProcessUpLinkFrame()
	go r.ProcessDownLinkFrame()

	for {
		switch reportState {
		case 0:
			{
				if r.GWLogin() == true {
					reportState = 1
					cronProcess.Start()
				} else {
					time.Sleep(5 * time.Second)
				}
			}
		case 1:
			{
				//网关
				if r.GWParam.ReportStatus == "offLine" {
					reportState = 0
					r.GWParam.ReportErrCnt = 0
					cronProcess.Stop()
				}
			}
		}

		time.Sleep(100 * time.Millisecond)
	}
}
